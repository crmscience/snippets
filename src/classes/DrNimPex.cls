public class DrNimPex {
	
    //  Property to hold # pieces still in gameplay
    public decimal Remaining { get { if (Remaining == null) Remaining = 12; return Remaining; } set; }
    
    //  List of "O" characters representing pieces
    public list<string> RemainingList { get { if (RemainingList == null) RemainingList = new list<string>{'O','O','O','O','O','O','O','O','O','O','O','O'}; return RemainingList; } set; }
    
    //  Property to hold list of previous moves
    public list<string> History { get { if (History == null) History = new list<string>(); return History; } set; }
    
    //  "Take X" Button Action
    public void DoTurn() {
        
        //  Determine which button the human player clicked on
        integer playerChoice = integer.valueOf(ApexPages.currentPage().getParameters().get('choice'));
		
        //  Update how many pieces are left
        Remaining -= playerChoice;
        
        //  Rebuild list
        RemainingList = new list<string>();
        for (integer i = 0; i < Remaining; i++)
            RemainingList.add('O');
        
            
        //  Prepare timeline update
        string newHistory = 'Player 1 played ' + playerChoice + ' resulting in: ';
        for (integer i = 0; i < Remaining; i++)
            newHistory += 'O,';
        
        //  Remove extra comma
        if (newHistory.endsWith(','))
            newHistory = newHistory.removeEnd(',');
        
        newHistory += ' -- ' + string.valueOf(remaining) + ' pieces left.';
        
        //  Update timeline
        History.add(newHistory);
        
        // Do Player 2 Turn
        DoPlayer2Turn();
    }
    
    //  Perform computer's turn
    public void DoPlayer2Turn() {
        
        //  Calculate AI's # of pieces to remove to win
        decimal player2Choice = (Remaining - ((((Remaining)/4).round(System.RoundingMode.UP))-1)*4);
        
        //  Update how many pieces are left
        Remaining -= player2Choice;
        
        //  Rebuild list
        RemainingList = new list<string>();
        for (integer i = 0; i < Remaining; i++)
            RemainingList.add('O');
        
        //  Prepare timeline update
        string newHistory = 'Player 2 played ' + player2Choice + ' resulting in: ';
        for (integer i = 0; i < Remaining; i++)
            newHistory += 'O,';
        
        //  Remove extra comma
        if (newHistory.endsWith(','))
            newHistory = newHistory.removeEnd(',');
        
        newHistory += ' -- ' + string.valueOf(remaining) + ' pieces left.';
        
        //  Update timeline
        History.add(newHistory);
        
        //  Add additional note if player 2 wins
        if (remaining == 0)
            History.add('Player 2 has won!');
    }
    
    //  "Play Again" button Action
    public void Reload() {
        Remaining = null;
        RemainingList = null;
        History = null;
    }
}